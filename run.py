#!/usr/bin/python

NAME = 'QtMusicPlayer'
VERSION = '0.0.1'

import sys
from qtmp.player import Server
from qtmp.interface.http.main import SiteMain
from PyQt4.Qt import QApplication
app = QApplication(sys.argv)
app.setApplicationName(NAME)
app.setApplicationVersion(VERSION)
server = Server(app)
server.add_interface(SiteMain)
server.start()
sys.exit(app.exec_())
