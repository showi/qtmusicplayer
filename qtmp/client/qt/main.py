from PyQt4 import QtGui
from qtmp.client.qt.ui.main import Ui_MainWindow
from PyQt4.Qt import QTimer
from qtmp.api import Api
from urllib.parse import quote_plus, quote, unquote

api = Api('localhost', 8080)


class Client(QtGui.QMainWindow, Ui_MainWindow):

    def __init__(self, parent=None):
        self.parent = parent
        QtGui.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.tableUrl.setEnabled(True)
        self.tableUrl.setColumnCount(1)
        self.tableUrl.clicked.connect(self.play)
        self.sliderPosition.setValue(0)
        self.progressBuffer.setValue(0)
        self.sliderVolume.setMaximum(100)
        self.sliderVolume.setValue(100)
        self.progressVolume.setMaximum(0)
        self.progressVolume.setMinimum(-60)
        self.progressVolume.setValue(100)
        self.labelPosition.setText('00:00')
        self.labelTitle.setText('')
        self.buttonAddUrl.clicked.connect(self.add_url)
        self.buttonStop.clicked.connect(self.stop)
        timer = QTimer(self)
        timer.timeout.connect(self.update)
        timer.start(5000)
        self.update_timer = timer

    def update(self):
        status = api.status()
        print("Status: %s" % status)
        if status is None or int(status['status']) != 0:
            print ('No update')
            return False
        self.update_status(status)

    def update_status(self, status):
        print(status)
        self.sliderPosition.setMaximum(status['total_time'])
        self.sliderPosition.setValue(int(status['current_time']))
        self.progressVolume.setValue(int(status['volume_decibel'])*10)
        self.sliderVolume.setValue(int(status['volume'])* 100)

    def add_url(self):
        print("Adding url")
        url, ok = QtGui.QInputDialog.getText(self, 'Enter URL', 'url:',
                                 QtGui.QLineEdit.Normal)
        if not ok:
            return False
        print("url: %s" % url)
        item = QtGui.QTableWidgetItem(url)
        item.setData(0, url)
#         item.setBackgroundColor(QColor.black)
#         item.setTextColor(QColor.green)
        row = self.tableUrl.rowCount()
        self.tableUrl.setRowCount(row)
        self.tableUrl.insertRow(row)
        self.tableUrl.setItem(row, 0, item)
        return True

    def play(self, index):
        item = self.tableUrl.item(index.row(), index.column())
        ret = api.play(quote_plus(item.text()))
        print("Return %s" % ret)
        return ret

    def stop(self):
        return api.stop()

if __name__ == '__main__':
    import sys
    app = QtGui.QApplication(sys.argv)
    c = Client(app)
    c.show()
    sys.exit(app.exec_())
