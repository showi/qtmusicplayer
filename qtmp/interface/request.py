import logging
log = logging.getLogger('qtmp.interface.request')
log.setLevel(logging.INFO)
import json
from qtmp.event import PlayEvent


class Request(object):

    keys = []
    data = None
    last_error = None
    is_valid = False

    def __init__(self, raw=None):
        raw = str(raw, encoding='utf8')
        if raw is not None:
            self.parse_json(raw)
#             if not isinstance(raw, str):
#                 self.data = raw
#             else:
#                 self.parse_json(raw)

    def parse_json(self, raw):
        log.info('RAW: %s', raw)
        self.data = None
        try:
            data = json.loads(raw)
        except Exception as e:
            print('Error: %s, raw: %s' % (e, raw))
            return False
        for key in data:
            if not self.check_key(data, key):
                return False
        self.data = data
        self.is_valid = True
        return True

    def __getattr__(self, name):
        log.info('NAME: %s, DATA: %s', name, self.data)
        if self.data is None:
            return None
        if not name in self.data:
            log.error('Invalid key %s', name)
            return None
        return self.data[name]

    def check_key(self, json, key):
        self.last_error = None
        if key not in self.keys:
            self.last_error = "Invalid key %s" % key
            return False
        elif key not in json:
            self.last_error = 'Missingkey %s' % key
            return False
        return True


class PlayRequest(Request):
    keys = PlayEvent.keys[:]
