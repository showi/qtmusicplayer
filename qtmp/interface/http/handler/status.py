import logging
log = logging.getLogger('qtmp.interface.http')
log.setLevel(logging.INFO)
import cherrypy  # @UnresolvedImport


from qtmp.event import StatusEvent, StatusEnum


def get_body_content():
    cl = cherrypy.request.headers['content-length']
    return cherrypy.request.body.read(int(cl))


class SiteStatus(object):

    exposed = True

    def __init__(self, parent):
        self.parent = parent

    def GET(self):
        req = StatusEvent()
#         self.parent.postEvent(req)
        res = self.parent.waitEvent(req, timeout=1)
        if res is None:
            req.status = StatusEnum.fail
            return req.to_json()
        return res.to_json()
