import logging
log = logging.getLogger('qtmp.interface.http')
log.setLevel(logging.INFO)
import cherrypy  # @UnresolvedImport

from qtmp.interface.http.response import PlayResponse, ResponseStatusEnum
from qtmp.event import PlayEvent
from qtmp.interface.request import PlayRequest


def get_body_content():
    cl = cherrypy.request.headers['content-length']
    return cherrypy.request.body.read(int(cl))


class SitePlay(object):

    exposed = True

    def __init__(self, parent):
        self.parent = parent

    def POST(self):
        request = PlayRequest(get_body_content())
        r = PlayResponse()
        r.url = request.url
        r.status = ResponseStatusEnum.error
        if not request.is_valid:
            r.msg = request.last_error
            return r.to_json()
        if request.url is None or request.url == '':
            r.msg = 'Missing URL'
            return r.to_json()
        r.status = ResponseStatusEnum.success
        event = PlayEvent()
        event.url = request.url
        log.info('POST URL: %s', event.url)
        res = self.parent.waitEvent(event)
        if res is None:
            print("no res")
            return event.to_json()
        res.to_json()
