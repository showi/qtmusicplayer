import logging
log = logging.getLogger('qtmp.interface.http')
log.setLevel(logging.INFO)
from qtmp.event import StopEvent


class SiteStop(object):

    exposed = True

    def __init__(self, parent):
        self.parent = parent

    def POST(self):
        event = StopEvent()
        res = self.parent.waitEvent(event)
        if res is None:
            return event.to_json()
        res.to_json()
