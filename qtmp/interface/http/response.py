import json


class ResponseStatusEnum(object):
    success = '200'
    error = '400'


class Response(object):

    keys = ['status', 'msg']

    def __init__(self, status=ResponseStatusEnum.success, msg=None):
        self.status = status
        self.msg = msg

    def to_json(self):
        data = {}
        for key in self.keys:
            data[key] = getattr(self, key)
        return json.dumps(data)

    def __str__(self):
        s = '<%s' % self.__class__.__name__
        s += 'status="%s", msg="%s"' % (self.status, self.msg)
        s += '>'
        return s


class PlayResponse(Response):
    url = None
    keys = Response.keys[:]
    keys.extend(('url', ))
