import logging
log = logging.getLogger('qtmp.interface.http')
from PyQt4.Qt import QThread, QMutex, QWaitCondition
import cherrypy  # @UnresolvedImport
from qtmp.interface.http.handler.play import SitePlay
from qtmp.interface.http.handler.stop import SiteStop
from qtmp.interface.http.handler.status import SiteStatus


VERSION = '0.0.1'


class SleepSimulator(QThread):

    mutex = QMutex()
    waitcondition = QWaitCondition()

    def __init__(self, timeout=3000):
        self.timeout = timeout
        self.mutex.lock()
        super(SleepSimulator, self).__init__()

    def sleep(self):
        self.waitcondition.wait(self.mutex, self.timeout)

    def cancel(self):
        print("Cancel lock")
        self.waitcondition.wakeAll()

    def run(self):
        self.sleep()
        self.quit()


class Interface(QThread):

    name = None

    def __init__(self, parent=None):
        if parent is None:
            log.warning('Creating Interface without parent')
        self.parent = parent
        super(Interface, self).__init__()
        self.installEventFilter(self)
        self.waiters = {}
        self.mutex = QMutex()

    def eventFilter(self, _obj, event):
        if event.type in self.waiters:
            for waiter in self.waiters[event.type]:
                waiter[1] = event.clone()
                waiter[0].wakeAll()
            del self.waiters[event.type]
        return False

    def postEvent(self, event=None, priority=1):
        if event is None:
            raise ValueError('Event cannot be None')
        self.parent.postSelfEvent(event, priority)

    def waitEvent(self, event, timeout=1, _priority=1):
        if not event.type in self.waiters:
            self.waiters[event.type] = []
        mutex = QMutex()
        mutex.lock()
        data = [QWaitCondition(), None]
        self.waiters[event.type].append(data)
        self.postEvent(event, 1)
        data[0].wait(mutex, timeout * 1000)
        return data[1]

conf = {
    'global': {
        'server.socket_host': '127.0.0.1',
        'server.socket_port': 8080,
        'engine.autoreload_on': False,
        'server.thread_pool': 3
    },

    '/': {
          'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
    }
}


class SiteMain(Interface):

    name = 'http_interface'

    def __init__(self, parent=None):
        super(SiteMain, self).__init__(parent)
        self.play = SitePlay(self)
        self.status = SiteStatus(self)
        self.stop = SiteStop(self)

    @cherrypy.expose
    def index(self):
        return "QtMp HTTP Interface (%s)" % VERSION

    def run(self):
        cherrypy.quickstart(self, '/', conf)

if __name__ == '__main__':
    NAME = 'QtMusicPlayer'
    VERSION = '0.0.1'

    import sys
    from PyQt4.Qt import QApplication
    app = QApplication(sys.argv)

    app.setApplicationName(NAME)
    app.setApplicationVersion(VERSION)
    root = SiteMain(app)
    root.start()
    sys.exit(app.exec_())

