import logging
log = logging.getLogger('qtmp.api')
import requests
from qtmp.event import StatusEvent, PlayEvent, StopEvent
import json


class ApiMethodEnum(object):
    GET = 0x1
    POST = 0x2
    DELETE = 0x3


def http_request(method, url, data=None):
    log.info("post %s %s" % (url, url))
    res = None
    if method == ApiMethodEnum.GET:
        res = requests.get(url)
    elif method == ApiMethodEnum.POST:
        res = requests.post(url, data)
    else:
        raise ValueError('Unknown method %s', method)
    if res.status_code != 200:
        print('Post fail')
        return None
    return res.json()


class ApiRequest(object):

    @classmethod
    def get(self, url):
        def wrapper(fn):
            def postit(myself):
                obj, event = fn(myself)
                curl = obj.base_url + url
                return http_request(ApiMethodEnum.GET, curl, event.to_json())
            return postit
        return wrapper

    @classmethod
    def post(self, url):
        def wrapper(fn):
            def postit(myself, *a, **ka):
                obj, event = fn(myself, *a, **ka)
                curl = obj.base_url + url
                return http_request(ApiMethodEnum.POST, curl, event.to_json())
            return postit
        return wrapper


class Api(object):

    base_url = 'http://localhost:8080'

    def __init__(self, host='localhost', port=None):
        self.host = host
        self.port = port
        if port is None:
            port = ''
        else:
            port = ':%s' % port
        self.base_url = 'http://%s%s' % (host, port)

    @ApiRequest.get('/status')
    def status(self):
        event = StatusEvent()
        return self, event

    @ApiRequest.post('/play')
    def play(self, url):
        event = PlayEvent()
        event.url = url
        return self, event

    @ApiRequest.post('/stop')
    def stop(self):
        event = StopEvent()
        return self, event
