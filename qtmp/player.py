import logging
log = logging.getLogger('qtmp.player')
from PyQt4.phonon import Phonon
from PyQt4.Qt import QThread, QMutex
from qtmp.event import EventTypeEnum  # , PlayEvent
try:
    from urllib.parse import unquote_plus  # @UnresolvedImport @UnusedImport
except:
    from urllib import unquote_plus  # @Reimport
from qtmp.event import StatusEvent, StatusEnum


class not_playing(object):

    def __init__(self, default=None):
        self.default = default

    def __call__(self, fn):
        def wrapped(myself, *a, **ka):
#             if not myself.is_playing():
#                 return self.default
            return fn(myself, *a, **ka)
        return wrapped


class Server(QThread):

    def __init__(self, parent):
        self.wait = QMutex()
        super(Server, self).__init__()
        self.parent = parent
        self.interfaces = {}
        widget = None
        mediaObject = Phonon.MediaObject(widget)
        self.audioOutput = Phonon.AudioOutput(Phonon.MusicCategory, widget)
        self.audioOutput.setVolume(1.0)
        Phonon.createPath(mediaObject, self.audioOutput)
        mediaObject.setTickInterval(500)
        #mediaObject.tick.connect(self.tock)
        #self.videoWidget = Phonon.VideoWidget(widget)
        #Phonon.createPath(mediaObject, self.videoWidget)
#         mediaObject.stateChanged.connect(self.on_state_changed)
#         mediaObject.aboutToFinish.connect(self.on_about_to_finish)
#         mediaObject.bufferStatus.connect(self.on_buffer_status)
#         mediaObject.setTransitionTime(-1)
#         self.mediaObject = mediaObject
        self.mediaObject = mediaObject
        self.installEventFilter(self)

    def add_interface(self, interface):
        if interface.name in self.interfaces:
            log.warning('Trying to insert interface with same name, skipping')
            return interface
        obj = interface(self)
        self.interfaces[interface.name] = obj
        obj.start()
        return interface

    def gen_status_event(self):
        status = StatusEvent()
        for key in status.keys:
            if key in ('type', 'status'):
                continue
            method = getattr(self, key)
            setattr(status, key, method())
        status.status = StatusEnum.success
        return status

    def eventFilter(self, obj, event):
        event = event.clone()
        status = self.gen_status_event()
        log.info('Event: %s, %s', obj, event)
        if event.type == EventTypeEnum.play:
            log.info('PLaying url %s', event.url)
            if event.url is None or event.url == '':
                pass
            else:
                if self.play(event.url):
                    status.status = StatusEnum.success
            self.postInterfaceEvent(status)
        elif event.type == EventTypeEnum.stop:
            if self.stop():
                status.status = StatusEnum.success
            self.postInterfaceEvent(event)
        elif event.type == EventTypeEnum.status:
            status.status = StatusEnum.success
            self.postInterfaceEvent(status)
        return False

    def postEvent(self, receiver=None, event=None, priority=1):
        if event is None:
            raise ValueError('Event cannot be None')
        if receiver is None:
            receiver = self
        #print("%s, %s" % (receiver, event))
        self.parent.postEvent(receiver, event, priority)

    def postSelfEvent(self, event, priority=1):
        self.postEvent(self, event, priority)

    def postInterfaceEvent(self, event, priority=1):
        for interface in self.interfaces:
            self.postEvent(self.interfaces[interface], event, priority)

    def play(self, url):
        log.info('Playing url %s', url)
        self.mediaObject.setCurrentSource(Phonon.MediaSource(unquote_plus(url)))
        self.mediaObject.play()

    def stop(self):
        if self.is_playing():
            self.mediaObject.stop()

    def pause(self):
        if self.is_playing():
            self.mediaObject.pause()

    @not_playing(0)
    def current_time(self):
        return self.mediaObject.currentTime()

    @not_playing(0)
    def total_time(self):
        return self.mediaObject.totalTime()

    @not_playing(0)
    def remaining_time(self):
        return self.mediaObject.remainingTime()

    @not_playing(0)
    def volume_decibel(self):
        return self.audioOutput.volumeDecibel()

    @not_playing(0)
    def volume(self):
        return self.audioOutput.volume()

    @not_playing(None)
    def url(self):
        return str(self.mediaObject.currentSource().url().toString())

    def state(self):
        return self.mediaObject.state()

    def is_playing(self):
        if self.state() == Phonon.PlayingState:
            return True
        return False

    def is_seakable(self):
        if self.is_playing():
            return False
        return self.mediaObject.isSeekable()

    def is_valid(self):
        return self.mediaObject.isValid()

    def title(self):
        return "CURRENT TITLE"

    def run(self):
        try:
            while True:
                log.info("Player is running")
                self.sleep(5)
        except KeyboardInterrupt:
            log("Got Ctrl-c, bye!")
