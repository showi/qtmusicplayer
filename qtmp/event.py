import json
from PyQt4.QtCore import QEvent


class EventTypeEnum(object):
    default = QEvent.registerEventType()
    media = QEvent.registerEventType()
    play = QEvent.registerEventType()
    stop = QEvent.registerEventType()
    status = QEvent.registerEventType()


class StatusEnum(object):
    success = 0x0
    fail = 0x1


class Event(QEvent):
    keys = ['type', 'status']
    type = EventTypeEnum.default
    status = StatusEnum.fail

    def __init__(self):
        super(Event, self).__init__(self.type)

    def to_json(self):
        data = {}
        for key in self.keys:
            data[key] = getattr(self, key)
        return json.dumps(data)

    def clone(self):
        obj = self.__class__()
        for key in self.keys:
            setattr(obj, key, getattr(self, key))
        return obj


class PlayEvent(Event):
    keys = Event.keys[:]
    keys.extend(['url'])
    type = EventTypeEnum.play
    url = None


class StopEvent(Event):
    type = EventTypeEnum.stop


class StatusEvent(Event):
    keys = Event.keys[:]
    keys.extend(('state', 'title', 'url', 'current_time', 'remaining_time',
                 'total_time', 'volume_decibel', 'volume'))
    type = EventTypeEnum.status
    state = None
    title = None
    url = None
    current_time = None
    remaining_time = None
    total_time = None
    volume = None
    volume_decibel = None


class MediaStateEnum(object):
    unknown = 0x0
    playing = 0x1
    stopped = 0x2
    paused = 0x3


class MediaEvent(Event):
    klass = 'media'
    keys = Event.keys[:]
    keys.extend(['state', 'url', 'title', 'duration'])
    type = EventTypeEnum.media
    state = MediaStateEnum.unknown
    url = None
    title = None
    duration = None
    position = None
