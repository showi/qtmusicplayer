from setuptools import setup, find_packages

setup(
    name="QtMusicPlayer",
    version="0.0.1",
    packages=find_packages(),
    scripts=[],
    install_requires=['requests'],
    package_data={},
    author="sho",
    author_email="",
    description="",
    license="GPL",
    keywords="",
    url="",
)
