export PYTHON=python2
ui_path=qtmp/client/qt/ui
dist=build/ dist/
pythton_version=2
clean_opt="pyc\|pyo\|log\|bak\|swp\|swo
all: ui

ui: 
	$(MAKE) -C ${ui_path}

win32:
	echo "clean $(clean_cmd)"

.PHONY: clean

clean:
	$(MAKE) -C $(ui_path) clean
	find . -name "*.pyc" -exec rm -v '{}' \;
	find . -name "*.pyo" -print -exec rm -v '{}' \;
	find . -name "*.log" -exec rm -v '{}' \;
	find . -name "*.swp" -exec rm -v '{}' \;
	rm -rf $(dist)
	rm -f data/twitpl.db
